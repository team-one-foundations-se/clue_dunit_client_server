#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <vector>

class QByteArray;

class CLogger
{
public:
    static CLogger* getInstance(const char* filename = nullptr);
    static void delInstance();
    void setAvatar(int a) { m_nAvatar = a; }
    int LogMessage(const char* fmt, ...);
    int LogQBA(QByteArray* pqba);
    int LogVector(std::vector<int>* pvec);
    int LogBuffer(const char*, int);

private:
    CLogger(const char*);
    ~CLogger();
    CLogger(const CLogger&);
    CLogger& operator=(const CLogger&);

    char* genTimeString(char*);

    static CLogger*  m_pThis;
    FILE*            m_fp;
    int              m_nAvatar;
};
