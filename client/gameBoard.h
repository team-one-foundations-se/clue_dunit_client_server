#pragma once

#include <QGraphicsView>
#include <QGraphicsItem>

#include "mainWnd.h"

const int houseSize = 27;

typedef struct _tag_loc
{
    int      id;                  // id of the location 0 - 8 are rooms to correlate to cards.
    int      type;                // 0 is room, 1 is hall, 2 is starting location
    const char*    name;
    QRect    bb;                  // bounding box of the location
    int      maxToons;            // max number of avatars in the location
    int      curToons;            // current count of occupants
    int      links[4];            // number of neighbors, -1 is not used
} locT, *plocT;

class QGraphicsScene;

class gameBoard : public QGraphicsView
{
    Q_OBJECT

public:
    gameBoard(QWidget* parent);
    int  mapToHouseIndex(int);               // converts room number ([0,8]) in to a house index

signals:
    void currentLoc(int);
    void sendSuggestion(int);
   

protected:

    void mousePressEvent(QMouseEvent*);
    void mouseReleaseEvent(QMouseEvent*);

private slots:
    void doUpdateState(QByteArray);

private:
    QGraphicsScene*       m_pScene;
    mainWnd*              m_pParent;          // back pointer to our parent

    QMap<int, QGraphicsItem*>   m_pTokens;    // map of avatar ID -> tokens.
    //QMap<int, QPoint>           m_pLocs;      // map of avatar -> current location
    QMap<QGraphicsItem*, int>   m_pRevTokens; // map of tokens -> avatar ID.

    int                        m_avatar;          // suspect we are working with
    int                        m_cntLocs;         // number of nodes in the graph house
    locT                       house[houseSize];  // graph representation of the house
    char                       m_nLocs[NBR_SUSPECTS]; // state where each suspect is (room number)
    QGraphicsItem*             m_pToken;          // token we are going to move
    int                        m_startLoc;        // starting location of the token
    int                        m_endingLoc;       // ending location of the token
    bool                       m_bMove;           // are we doing a move

    // functions to draw map...
    void              initHouse();
    void              drawPlayers();
    void              placeLabels();

    // functions to manage moves...
    bool              validMove();
    void              updateLocation(int, int);   // moves a single token to a room

    // debugging functions ....
    void              highlight();
    void              drawGrid();


};
