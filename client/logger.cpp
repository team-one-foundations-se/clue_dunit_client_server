#include "logger.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>                     // windows specific for va_list and friends
#include <time.h>

#include <QByteArray>

CLogger* CLogger::m_pThis = nullptr;

const char* defName = ".\\client.log";
const char* lpszMonths[]= {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
const int  timeLen = 19;


CLogger* CLogger::getInstance(const char* filename)
{
    if (m_pThis == nullptr)
        m_pThis = new CLogger(filename);

    return m_pThis;

}

void CLogger::delInstance()
{
    if (m_pThis != nullptr)
        delete m_pThis;

}

   

int CLogger::LogMessage(const char* fmt, ...)
{
    int cntPrinted = 0;
    char dtbuf[timeLen];
 
    va_list   va;
    va_start(va, fmt);

    int strlen = _vscprintf(fmt, va);                   // get space required for substitution
    char* mbuf = new char[strlen + 1];                  // allocate space for our message 
    char* buf = new char[strlen + timeLen + 1];         // allocate space for our final message
    cntPrinted = vsprintf(mbuf, fmt, va);               // construct log message
    
    strcpy(buf, genTimeString(&dtbuf[0]));
    strcat(buf, mbuf);


    va_end(va);
    fprintf(m_fp, "%d - %s\n", m_nAvatar, buf);
    delete[] buf;
    return cntPrinted;
}


int CLogger::LogQBA(QByteArray* pqba)
{
    int ret;
    try
    {
        char* szBuf = new char[5 * pqba->length() + 5];           // allocate enough space for buffer
        sprintf(szBuf, "[");
        for (int ndx = 0; ndx < pqba->length(); ndx++)
        {
            char  szTemp[11];
            sprintf(szTemp, "0x%02X ", (unsigned char)pqba->at(ndx));
            strcat(szBuf, szTemp);
        }
        strcat(szBuf, "]");

        ret = LogMessage("%s\n", szBuf);

        delete szBuf;
    }
    catch (std::bad_alloc exc)
    {
        LogMessage("%s", "failed to allocate memory in LogQBA\n");
    }
    return ret;
}

int CLogger::LogBuffer(const char* buf, int len)
{
    int ret;
    try
    {
        char* szBuf = new char[5 * len + 5];           // allocate enough space for buffer
        sprintf(szBuf, "[");
        for (int ndx = 0; ndx < len; ndx++)
        {
            char  szTemp[11];
            sprintf(szTemp, "0x%02X ", (unsigned char)buf[ndx]);
            strcat(szBuf, szTemp);
        }
        strcat(szBuf, "]");

        ret = LogMessage("%s\n", szBuf);

        delete szBuf;
    }
    catch (std::bad_alloc exc)
    {
        LogMessage("%s", "failed to allocate memory in LogQBA\n");
    }
    return ret;
}

// we expect no more than 18 cards (single player, debugging), number no larger than 30
// so we would need four spaces for an entry (##, ) thus we need a total of 75 characters.
int CLogger::LogVector(std::vector<int>* pvec)
{
    int   ret;
    try
    {
        int size = pvec->size();
        char* szBuf = new char[128];

        sprintf(szBuf, "[");
        for (int ndx = 0; ndx < pvec->size(); ndx++)
        {
            char  szTemp[6];
            sprintf(szTemp, "0x%02d, ", pvec->at(ndx));
            strcat(szBuf, szTemp);
        }
        strcat(szBuf, "]");

        ret = LogMessage("%s\n", szBuf);

        delete szBuf;
    }
    catch (std::bad_alloc exc)
    {
        LogMessage("%s", "failed to allocate memory in LogQBA\n");
    }

    return ret;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
char* CLogger::genTimeString(char* pdtbuf)
{
    struct tm* tm;
    __time64_t long_time;

    memset((void*)pdtbuf, '/0', timeLen);

    // build date/time buffer: [dd-mmm-yyyy : hhmm]        20-character
    _time64(&long_time);                                  // Get time as 64-bit integer.
    tm = _localtime64(&long_time);
    sprintf(pdtbuf, "[%02d-%03s-%04d:%02d%02d] ", tm->tm_mday, lpszMonths[tm->tm_mon], 1900 + tm->tm_year, tm->tm_hour, tm->tm_min);

    return pdtbuf;
}

// These function are private so they can not be invoked by accident.
CLogger::CLogger(const char* filename) 
{ 
    m_fp = fopen(filename, "a");
    if (nullptr == m_fp)
    {
        // TODO : error happened... should not attempt anymore logging functions.
        // TODO : throw an exception here...
    }

}

CLogger::~CLogger()
{
    if (nullptr != m_fp)
        fclose(m_fp);
}


