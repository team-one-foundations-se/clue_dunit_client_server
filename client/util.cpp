#include "util.h"

#include <iostream>

#include <QByteArray>

void printBuffer(const char* buf, int len)
{
    std::cout << " [";
    for (int ndx = 0; ndx < len; ndx++)
    {
        fprintf(stdout, "0x%02X ", buf[ndx]);
    }
    std::cout << "]" << std::endl;
}


void printQBA(QByteArray* pqba)
{
    std::cout << "[";
    for (char c : *pqba)
    {
        fprintf(stdout, "0x%02x ", c);
    }
    std::cout << "]" << std::endl;
    
}